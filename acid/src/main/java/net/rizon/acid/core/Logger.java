package net.rizon.acid.core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

final class loggerHandler extends Handler
{
	private static final DateFormat format = new SimpleDateFormat("EEE MMM dd kk:mm:ss:SSSS yyyy");

	public loggerHandler()
	{
		this.setLevel(Level.ALL);
	}

	@Override
	public void close() throws SecurityException { }

	@Override
	public void flush() { }

	@Override
	public void publish(LogRecord record)
	{
		String message = record.getMessage();
		StackTraceElement[] stes = (record.getThrown() != null ? record.getThrown().getStackTrace() : null);
		boolean bad = record.getLevel() == Level.SEVERE || record.getLevel() == Level.WARNING || record.getLevel() == Level.ALL || record.getLevel() == Level.CONFIG;

		if (record.getLevel() == Level.INFO || bad)
			if (AcidCore.me != null && !AcidCore.me.isBursting() && User.findUser(Acidictive.conf.general.control) != null)
			{
				if (message != null)
					Acidictive.privmsg(Acidictive.conf.getChannelNamed("routing-spam"), message);
				if (stes != null)
				{
					Acidictive.privmsg(Acidictive.conf.getChannelNamed("routing-spam"), record.getThrown().toString());
					for (StackTraceElement ste : stes)
						Acidictive.privmsg(Acidictive.conf.getChannelNamed("routing-spam"), ste.toString());
				}
			}

		if (Acidictive.conf == null || Acidictive.conf.debug || bad)
		{
			System.out.println(format.format(new Date(record.getMillis())) + " [" + record.getLevel().getName() + "] [" + record.getLoggerName() + "] " + message);
			if (stes != null)
			{
				System.out.println(record.getThrown().toString());
				for (StackTraceElement ste : stes)
					System.out.println(ste.toString());
			}
		}
	}
}

public class Logger extends java.util.logging.Logger
{
	private static HashMap<String, Logger> loggers = new HashMap<String, Logger>();
	private static final loggerHandler handler = new loggerHandler();

	protected Logger(String name, String resourceBundleName)
	{
		super(name, resourceBundleName);
	}

	public static Logger getLogger(final String name)
	{
		Logger l = loggers.get(name);
		if (l != null)
			return l;

		l = new Logger(name, null);
		loggers.put(name, l);

		l.setLevel(Level.ALL);
		l.addHandler(handler);
		return l;
	}

	public void log(Exception ex)
	{
		this.log(Level.SEVERE, null, ex);
	}
}