package net.rizon.acid.plugins.trapbot;

import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Event;
import net.rizon.acid.core.Protocol;
import net.rizon.acid.core.User;

class TrapEvent extends Event
{
	@Override
	public void onUserConnect(User u)
	{
		TrappedUser t = trapbot.getTrappedUser(u);
		/* force the user back in */
		if (t != null)
		{
			t.update();
			Protocol.svsjoin(u, trapbot.getTrapChanName());
		}
	}

	@Override
	public void onJoin(Channel channel, final User[] users)
	{
		if (!channel.getName().equalsIgnoreCase(trapbot.getTrapChanName()))
			return;

		for (User u : users)
		{
			trapbot.updateTrap(u);
		}
	}

	@Override
	public void onPart(User u, Channel channel)
	{
		if (!channel.getName().equalsIgnoreCase(trapbot.getTrapChanName()))
			return;

		if (u.hasMode("o"))
			return;

		TrappedUser tu = trapbot.getTrappedUser(u);
		if (tu == null)
			tu = trapbot.makeTrappedUser(u);

		/*
		 * if enforce is disabled, we let the parting users go
		 */
		if (trapbot.enforce)
		{
			tu.update();
			Protocol.svsjoin(u, trapbot.getTrapChanName());
		}
		else
			trapbot.freeUser(u);
	}

	public void onKick(String kicker, User victim, String channel, String reason)
	{
		if (!channel.equalsIgnoreCase(trapbot.getTrapChanName()))
			return;

		trapbot.freeUser(victim);
	}
}