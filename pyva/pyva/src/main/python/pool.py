import threading
import MySQLdb as db

class DBPool(object):
	_lock = threading.RLock()
	_connections = []

	def __init__(self, conf):
		self.config = conf
		self.add_connection()

	def add_connection(self):
		con = db.connect(
			host=self.config.get('database').get('host'),
			user=self.config.get('database').get('user'),
			passwd=self.config.get('database').get('passwd'),
			db=self.config.get('database').get('db'),
		)
		con.ping(True)
		con.autocommit(True) #no need to have transactions

		with self._lock:
			self._connections.append(con)

	def get_connection(self):
		with self._lock:
			if len(self._connections) == 0:
				self.add_connection()
			return self._connections.pop()

	def put_connection(self, con):
		with self._lock:
			if con not in self._connections:
				self._connections.append(con)
		

