from urllib import urlencode
from feed import XmlFeed

class Bing(object):
	def __init__(self, key):
		self.API_KEY = key
		f = XmlFeed('http://api.microsofttranslator.com/V2/Http.svc/GetLanguagesForTranslate?appId=' + self.API_KEY)
		self.languages = [lang.childNodes[0].nodeValue.lower() for lang in f.elements('ArrayOfstring')[0]._element.childNodes]
		
	def translate(self, text, source=None, target='en'):
		url = 'http://api.microsofttranslator.com/V2/Http.svc/Translate?'
		url += urlencode({'text': text,
						'to': target,
						'appId': self.API_KEY})
		if source:
			url += '&from=' + source
		
		xml = XmlFeed(url)
		return xml.text('string')
	
	def detect_language(self, text):
		url = 'http://api.microsofttranslator.com/V2/Http.svc/Detect?'
		url += urlencode({'text': text,
						'appId': self.API_KEY})
		
		xml = XmlFeed(url)
		return xml.text('string')
