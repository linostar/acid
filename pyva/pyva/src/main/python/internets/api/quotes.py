from datetime import datetime
from feed import XmlFeed
from utils import unescape
from xml.dom import minidom
import urllib


class Quotes(object):
	def __init__(self, key_fml):
		self.key_fml = key_fml
		self.fml_cache = {}  # xml-elements  # 10-quotes cache
		self.qdb_cache = {}  # {'quotes': xml-elements, 'ts': datetime-timestamp, 'next': next quote index} // page cached for 15s

	def get_qdb_random(self):
		if not self.qdb_cache or (datetime.now() - self.qdb_cache['ts']).seconds > 15:
			qdb = XmlFeed('http://qdb.us/qdb.xml?action=random&fixed=0')
			quotes = qdb.elements('//item')
			next = 0
			self.qdb_cache = {'quotes': quotes, 'ts': datetime.now(), 'next': next}
		else:
			quotes = self.qdb_cache['quotes']
			next = self.qdb_cache['next']

		quote = quotes[next]
		self.qdb_cache['next'] = next + 1
		lines = unescape(quote.text('description')).split('<br />')
		if len(lines) > 5:
			return self.get_qdb_random()
		else:
			return {'lines': lines, 'link': quote.text('link'), 'id': quote.text('title')}

	def get_qdb_id(self, quote_id):
		qdb = XmlFeed('http://qdb.us/qdb.xml?action=quote&quote=%d&fixed=0' % quote_id)
		quotes = qdb.elements('//item')
		if quotes:
			quote = quotes[0]
			lines = unescape(quote.text('description')).split('<br />')
			if len(lines) > 5:
				return {'lines': ['this quote is too long. You can view it here: %s' % quote.text('link')], 'link': quote.text('link'), 'id': quote.text('title')}
			else:
				return {'lines': lines, 'link': quote.text('link'), 'id': quote.text('title')}
		else:
			return None

	def _get_fml_quotes(self, url):
		feed = minidom.parse(urllib.urlopen(url))
		return feed.getElementsByTagName('item')

	def get_fml(self, quote_id=None):
		"""Grab either the given quote (by id), or just a random one."""
		if quote_id is None:  # if we're doing a random quote
			# for random quotes, we request 10 at a time, then repopulate our cache when it runs out
			if not self.fml_cache:
				try:
					self.fml_cache = self._get_fml_quotes('http://api.fmylife.com/view/random/10/nocomment?key=%s&language=en' % self.key_fml)					
				except Exception as e:
					raise FmlException("fmylife.com is temporarily unavailable. Please try again later.")
				
				if len(self.fml_cache) == 0:
					raise FmlException("fmylife.com is temporarily unavailable. Please try again later.")

			quote = self.fml_cache.pop()

		else:
			quote = self._get_fml_quotes('http://api.fmylife.com/view/%d/nocomment?key=%s&language=en' % (quote_id, self.key_fml))[0]

		text = quote.getElementsByTagName('text')[0].toxml()[6:-7]
		id = quote.getAttribute('id')
		category = quote.getElementsByTagName('category')[0].toxml()[10:-11]

		return {'text': unescape(text), 'id': id, 'category': category}


class FmlException(Exception):
	def __init__(self, e):
		self.msg = e

	def __str__(self):
		return self.msg
