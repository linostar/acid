#!/usr/bin/python pseudoserver.py
# psm_quotes.py
# based on psm_limitserv.py written by celestin - martin <martin@rizon.net>

import sys
import types
import random
from istring import istring
from pseudoclient import sys_log, sys_options, sys_channels, inviteable
from utils import *

from pyva import *
import logging
from core import *
from plugin import *

import cmd_admin, sys_auth

import pyva_net_rizon_acid_core_Acidictive as Acidictive
import pyva_net_rizon_acid_core_AcidCore as AcidCore
import pyva_net_rizon_acid_core_User as User
import pyva_net_rizon_acid_core_Channel as Channel

class quotes(
	AcidPlugin,
	inviteable.InviteablePseudoclient
):
	initialized = False
	
	def start_threads(self):
		self.options.start()
		self.channels.start()
		self.auth.start()
		
	def bind_function(self, function):
		func = types.MethodType(function, self, quotes)
		setattr(quotes, function.__name__, func)
		return func

	def bind_admin_commands(self):
		list = cmd_admin.get_commands()
		self.commands_admin = []

		for command in list:
			self.commands_admin.append((command, {'permission': 'j', 'callback': self.bind_function(list[command][0]), 
												'usage': list[command][1]}))

	def __init__(self):
		AcidPlugin.__init__(self)

		self.name = "quotes"
		self.log = logging.getLogger(__name__)

		try:
			self.nick = istring(self.config.get('quotes').get('nick'))
		except Exception, err:
			self.log.exception("Error reading 'quotes:nick' configuration option: %s" % err)
			raise

		try:
			self.chan = istring(self.config.get('quotes').get('channel'))
		except Exception, err:
			self.log.exception("Error reading 'quotes:channel' configuration option: %s" % err)
			raise

		try:
			self.outputlimit = int(self.config.get('quotes').get('outputlimit'))
		except Exception, err:
			self.log.exception("Error reading 'quotes:output-limit' configuration option: %s" % err)
			raise
		
		self.bind_admin_commands()
					
	def start(self):
		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS quotes_chans (id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT, name VARCHAR(200) NOT NULL, UNIQUE KEY(name)) ENGINE=MyISAM;")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS quotes_quotes (id INT(10) NOT NULL, quote VARCHAR(512) NOT NULL, nick VARCHAR(30) NOT NULL, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, channel INT NOT NULL, KEY `chan_index` (`channel`)) ENGINE=MyISAM;")
		except Exception, err:
			self.log.exception("Error creating table for quotes module (%s)" % err)
			raise
		
		try:
			AcidPlugin.start(self)
			inviteable.InviteablePseudoclient.start(self)

			self.options = sys_options.OptionManager(self)
			self.elog = sys_log.LogManager(self)
			self.auth = sys_auth.QuotesAuthManager(self)
			self.channels = sys_channels.ChannelManager(self)
		except Exception, err:
			self.log.exception('Error initializing subsystems for quotes module (%s)' % err)
			raise

		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.elog.debug('Joined channels.')
		
		try:
			self.start_threads()
		except Exception, err:
			self.log.exception('Error starting threads for quotes module (%s)' % err)
			raise

		self.initialized = True
		self.elog.debug('Started threads.')
		return True
	
	def stop(self):
		if hasattr(self, 'auth'):
			self.auth.stop()

		if hasattr(self, 'channels'):
			if self.initialized:
				self.channels.force()

			self.channels.stop()
			self.channels.db_close()

		if hasattr(self, 'options'):
			if self.initialized:
				self.options.force()

			self.options.stop()
			self.options.db_close()

	def join(self, channel):
		super(quotes, self).join(channel)
		self.dbp.execute("INSERT IGNORE INTO quotes_chans(name) VALUES(%s)", (str(channel),))

	def part(self, channel):
		super(quotes, self).part(channel)

		self.dbp.execute("DELETE FROM quotes_quotes WHERE channel=%s", (self.get_cid(channel),))
		self.dbp.execute("DELETE FROM quotes_chans WHERE name=%s", (str(channel),))

	def msg(self, target, message):
		if message != '':
			Acidictive.privmsg(self.nick, target, format_ascii_irc(message))

	def multimsg(self, target, count, intro, separator, pieces, outro = ''):
		cur = 0

		while cur < len(pieces):
			self.msg(target, intro + separator.join(pieces[cur:cur + count]) + outro)
			cur += count
		
	def notice(self, target, message):
		if message != '':
			Acidictive.notice(self.nick, target, format_ascii_irc(message))
	
	def get_cid(self, cname):
		"""Fetches the channel id for a given channel name."""
		self.dbp.execute("SELECT id FROM quotes_chans WHERE name = %s", (str(cname),))
		cid = self.dbp.fetchone()[0]
		return cid

	def read_quote(self, qid, cid, cname):
		"""Fetches the quote id qid from the channel with id cid and displays
		the result in the channel cname. cname is to be passed on so we can
		avoid querying the db about the name, which we have in all scenarios
		where read_quote is called anyway."""
		qid = str(qid)
		num = self.dbp.execute("SELECT nick, quote, time FROM quotes_quotes WHERE id = %s AND channel = %s",
				(qid, cid))

		if num == 0:
			self.msg(cname, "[Quote] " + qid + " does not exist!")
			return

		nick, quote, time = self.dbp.fetchone()
		tdelta = int((datetime.now() - time).total_seconds())
		tdeltastr = ""
		years = weeks = days = hours = minutes = 0

		while tdelta > 31540000:
			years += 1
			tdelta -= 31540000
		while tdelta > 604800:
			weeks += 1
			tdelta -= 604800
		while tdelta > 86400:
			days += 1
			tdelta -= 86400
		while tdelta > 3600:
			hours += 1
			tdelta -= 3600
		while tdelta > 60:
			minutes += 1
			tdelta -= 60

		if years > 0:
			tdeltastr += str(years) + " year"
			if years != 1:
				tdeltastr += "s"
			tdeltastr += " "
		if weeks > 0:
			tdeltastr += str(weeks) + " week"
			if weeks != 1:
				tdeltastr += "s"
			tdeltastr += " "
		if days > 0:
			tdeltastr += str(days) + " day"
			if days != 1:
				tdeltastr += "s"
			tdeltastr += " "
		if hours > 0:
			tdeltastr += str(hours) + " hour"
			if hours != 1:
				tdeltastr += "s"
			tdeltastr += " "
		if minutes > 0:
			tdeltastr += str(minutes) + " minute"
			if minutes != 1:
				tdeltastr += "s"
			tdeltastr += " "
		if tdelta > 0:
			tdeltastr += str(tdelta) + " second"
			if tdelta != 1:
				tdeltastr += "s"

		self.msg(cname, "[Quote] #%s added by %s %s ago." % (qid, nick, tdeltastr))
		Acidictive.privmsg(self.nick, cname, '[Quote] %s' % quote)
		return

	def onPrivmsg(self, source, target, message):
		# Parse ADD/DEL requests
		if not self.initialized:
			return

		# HACKY
		# if inviteable didn't catch the command, it means we can handle it here instead
		if not inviteable.InviteablePseudoclient.onPrivmsg(self, source, target, message):
			return

		myself = User.findUser(self.nick)
		channel = target

		userinfo = User.findUser(source)
		sender = userinfo['nick']

		msg = message.strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			arg = ''
		else:
			command = msg[:index]
			arg = msg[index + 1:]

		command = command.lower()
		
		if self.channels.is_valid(channel) and command.startswith("."): # a channel message
			command = command[1:]
			if command == 'help' and arg == '':
				self.notice(sender, "Quotes: .help quotes - for quote commands.")
			elif command == 'help' and arg.startswith('quotes'):
				self.notice(sender, "Quotes: .quote add <quote> - adds given quote to database, must have voice or higher on channel.")
				self.notice(sender, "Quotes: .quote del <number> - removes quote number from database, must be channel founder.")
				self.notice(sender, "Quotes: .quote search <query> - searches database for given query.")
				self.notice(sender, "Quotes: .quote read <number> - messages quote matching given number.")
				self.notice(sender, "Quotes: .quote random - messages a random quote.")
				self.notice(sender, "Quotes: .quote total - messages number of quotes in database.")
			elif command == 'quote':
				args = arg.split(' ')
				cid = self.get_cid(channel)

				if args[0] == 'add':
					c = Channel.findChannel(channel)
					memb = c.findUser(userinfo)
					# Voice is the lowest rank, i.e., we can just check for emptiness
					if not memb or memb.getModes() == "":
						self.notice(sender, "You must have voice or higher on the channel to add quotes.")
						return
					quote = istring(' '.join(args[1:]))

					# Extremely inefficient. If possible, somehow merge into
					# just one MySQL query
					# MAX(id) may return NULL if (and only if) there are no results, i.e., 0
					# quotes. Work around that with IFNULL(expr,elsevalue).
					self.dbp.execute("SELECT IFNULL(MAX(id)+1,1) FROM quotes_quotes WHERE channel = %s", (cid,))
					qid = self.dbp.fetchone()[0]

					self.dbp.execute("INSERT INTO quotes_quotes(id, quote, nick, channel) VALUES(%s, %s, %s, %s)",
							(qid, str(quote), sender, cid))

					self.msg(channel, "[Quote] Added quote #%d by %s" % (qid, sender))
					Acidictive.privmsg(self.nick, channel, '[Quote] %s' % quote)

				if args[0] == 'del':
					if len(args) > 1 and not args[1].isdigit():
						self.notice(sender, args[1] + " is not a valid quote number.")
						return

					if len(args) == 1:
						self.notice(sender, "Please specify quote number to delete.")
						return

					self.notice(sender, "Checking if you are the channel founder.")
					self.auth.request(sender, channel, 'delete_quote' + args[1])

				if args[0] == 'search':
					if len(args) < 2:
						return

					num = self.dbp.execute("SELECT id FROM quotes_quotes WHERE channel = %s AND quote LIKE CONCAT('%%',%s,'%%')",
							(cid, ' '.join(args[1:]) ))
					res = self.dbp.fetchall()
					if num == 0:
						self.msg(channel, "[Quote] No quotes found.")
						return
					if num > 1:
						ids = []
						for row in res:
							ids.append(str(row[0]))
						if num > self.outputlimit:
							self.msg(channel, "[Quote] The search returned a lot of results, output will be sent by notice.")
							self.notice(sender, "[Quote] {0} matches found: #{1}".format(num, ','.join(ids)))
						else:
							self.msg(channel, "[Quote] {0} matches found: #{1}".format(num, ','.join(ids)))
						return

					self.read_quote(res[0][0], cid, channel)

				if args[0] == 'read':
					if len(args) < 2:
						return

					self.read_quote(args[1], cid, channel)
					
				if args[0] == 'random':
					self.dbp.execute("SELECT id FROM quotes_quotes WHERE channel = %s ORDER BY RAND() LIMIT 1", (cid,))
					res = self.dbp.fetchall()

					if not res:
						self.msg(channel, "[Quote] No quotes found!")
						return

					self.read_quote(res[0][0], cid, channel)

				if args[0] == 'total':
					self.dbp.execute("SELECT COUNT(id) FROM quotes_quotes WHERE channel = %s", (cid,))
					qtotal = self.dbp.fetchone()[0]
					
					if qtotal != 1:
						self.msg(channel, "[Quote] %d quotes in total" % qtotal)
					else:
						self.msg(channel, "[Quote] %d quote in total" % qtotal)

				if args[0] == 'last':
					self.dbp.execute("SELECT id FROM quotes_quotes where channel = %s ORDER BY id DESC LIMIT 1", (cid,))
					res = self.dbp.fetchall()

					if not res:
						self.msg(channel, "[Quote] No quotes found!")
						return

					self.read_quote(res[0][0], cid, channel)

			return
	
	def delete_quote(self, channel, user, qid):
		cid = self.get_cid(channel)

		self.dbp.execute("DELETE FROM quotes_quotes WHERE id = %s AND channel = %s", (qid, cid))
		self.notice(user, "Deleted quote #%d" % qid)
		return

	def onChanModes(self, prefix, channel, modes):
		if not self.initialized:
			return

		if not modes == '-z':
			return

		if channel in self.channels:
			self.channels.remove(channel)
			try:
				self.dbp.execute("DELETE FROM quotes_quotes WHERE channel = %s", (self.get_cid(channel),))
			except:
				pass
			self.dbp.execute("DELETE FROM quotes_chans WHERE name = %s", (channel,))
			self.elog.request('Channel @b%s@b was dropped. Deleting it.' % channel)
	
	def getCommands(self):
		return self.commands_admin
